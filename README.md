###### SpringCloud学习示例

> 开发环境   

- JDK1.8
- SpringBoot 1.5.8.RELEASE
- SpringCloud Dalston.SR5   

> SpringCloud示例  

1. [x] 	[ 	eureka、ribbon搭建简单服务提供、注册中心、服务消费示例](https://github.com/DemoOfBug/spring-cloud-samples/tree/master/sample1 "eureka、ribbon搭建简单服务提供、注册中心、服务消费示例")