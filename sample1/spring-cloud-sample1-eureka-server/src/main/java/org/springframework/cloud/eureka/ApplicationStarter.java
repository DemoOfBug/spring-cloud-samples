package org.springframework.cloud.eureka;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Bean;

import lombok.extern.slf4j.Slf4j;

@EnableEurekaServer
@SpringBootApplication
@Slf4j
public class ApplicationStarter {
	public static void main(String[] args) {
		SpringApplication.run(ApplicationStarter.class, args);
	}

	@Bean
	public ApplicationRunner applicationRunner() {
		return (e) -> {
			log.info("Server running at http://localhost:8090");
		};
	}
}
