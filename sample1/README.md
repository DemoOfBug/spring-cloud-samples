###### eureka、ribbon搭建简单服务提供、注册中心、服务消费示例

> 项目结构   

- spring-cloud-sample1-eureka-server  注册中心
- spring-cloud-sample1-provider          服务提供者
- spring-cloud-sample1-ribbon-consumer	 服务消费者  

> maven依赖配置

1. spring-cloud-sample1-eureka-server(注册中心)  
```
	<dependencies>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka-server</artifactId>
		</dependency>
	</dependencies>
```  
2. spring-cloud-sample1-provider(服务提供者)  
```
        <dependencies>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka</artifactId>
		</dependency>
	</dependencies>
```
3.  spring-cloud-sample1-ribbon-consumer(服务消费者)  
```
        <dependencies>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-ribbon</artifactId>
		</dependency>
	</dependencies>
```

> 启动顺序  
 
1. spring-cloud-sample1-eureka-server(注册中心)  
2.  spring-cloud-sample1-provider(服务提供者)   
2.  spring-cloud-sample1-ribbon-consumer(服务消费者)    

> 启动成功访问地址  

http://localhost:8091/ribbon-consumer
