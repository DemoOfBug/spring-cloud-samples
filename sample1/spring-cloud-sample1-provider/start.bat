@echo off

:number
set /p number=请输入服务编号(0-9):
if "%number%"=="" goto number
title 服务%number%

:port
set /p port=请输入服务编号(0-65535):
if "%port%"=="" goto port
echo 启动端口：%port%


echo 开始启动服务
mvn spring-boot:run -Drun.jvmArguments='-Dserver.port=%port%'
pause