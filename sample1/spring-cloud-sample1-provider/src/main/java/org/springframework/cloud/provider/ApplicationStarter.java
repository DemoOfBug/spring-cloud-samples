package org.springframework.cloud.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import lombok.extern.slf4j.Slf4j;

@EnableDiscoveryClient
@SpringBootApplication
@Slf4j
public class ApplicationStarter {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ApplicationStarter.class, args);
	}
	
	@Bean
	public ApplicationListener<EmbeddedServletContainerInitializedEvent> listener() {
		return (EmbeddedServletContainerInitializedEvent e) -> {
			int port = e.getEmbeddedServletContainer().getPort();
			log.info(String.format("Server running at http://localhost:%d/hello", port));
		};
	}
}
