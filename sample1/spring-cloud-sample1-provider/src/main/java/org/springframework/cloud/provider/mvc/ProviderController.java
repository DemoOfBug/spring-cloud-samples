package org.springframework.cloud.provider.mvc;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ProviderController {
	private final Random random = new Random();
	
	@Autowired
	private DiscoveryClient discoveryClient;
	
	@RequestMapping(value = "/hello")
	public ServiceInstance index()throws Exception
	{
		ServiceInstance si = discoveryClient.getLocalServiceInstance();
		long sleepTime = random.nextInt(3000);
		Thread.sleep(sleepTime);
		log.info("It has been waiting for "+sleepTime+" ms");
		return si;
	}
}
